// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.8.1 (swiftlang-5.8.0.124.5 clang-1403.0.22.11.100)
// swift-module-flags: -target x86_64-apple-ios11.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name Liveness
// swift-module-flags-ignorable: -enable-bare-slash-regex
import AVFoundation
import Accelerate
import CommonCrypto
import CoreImage
import Foundation
@_exported import Liveness
import Swift
import SystemConfiguration
import UIKit
import _Concurrency
import _StringProcessing
public enum FaceTypeStatus {
  case LOW_BRIGHTNESS
  case HIGH_BRIGHTNESS
  case TOO_FAR
  case TOO_CLOSE
  case LIES_OUTSIDE
  case MULTIPLE_FACES
  case MOTION_DETECT
  case NO_FACE
  case POOR_QUALITY
  public static func == (a: Liveness.FaceTypeStatus, b: Liveness.FaceTypeStatus) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public protocol KLivenessCallbacks : AnyObject {
  func showLoader(cb: (() -> Swift.Void)?)
  func hideLoader(cb: (() -> Swift.Void)?)
  func onReceiveKLiveResult(status: Liveness.KLiveStatus, result: Liveness.KLiveResult?)
  func faceStatus(faceStatus: Liveness.FaceStatus, faceTypeStatus: Liveness.FaceTypeStatus?)
}
public enum Brightness : Swift.Int {
  case LOW
  case HIGH
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@objc @_inheritsConvenienceInitializers @_Concurrency.MainActor(unsafe) public class KLivenessViewController : UIKit.UIViewController {
  @_Concurrency.MainActor(unsafe) public var colorFace: UIKit.UIColor
  @_Concurrency.MainActor(unsafe) public var colorNoFace: UIKit.UIColor
  @_Concurrency.MainActor(unsafe) public var colorInvalidFace: UIKit.UIColor
  @_Concurrency.MainActor(unsafe) public var backgroundColor: UIKit.UIColor
  @_Concurrency.MainActor(unsafe) public var lensFacing: AVFoundation.AVCaptureDevice.Position
  @_Concurrency.MainActor(unsafe) public var timeoutSeconds: Swift.Int
  @_Concurrency.MainActor(unsafe) convenience public init(delegate: any Liveness.KLivenessCallbacks, karzaToken: Swift.String, environment: Liveness.KEnvironment, details: [Swift.String : Any]?, cameraFacing: AVFoundation.AVCaptureDevice.Position)
  @_Concurrency.MainActor(unsafe) @objc override dynamic public func viewDidLoad()
  @objc deinit
  @_Concurrency.MainActor(unsafe) @objc override dynamic public init(nibName nibNameOrNil: Swift.String?, bundle nibBundleOrNil: Foundation.Bundle?)
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder: Foundation.NSCoder)
}
public enum KLiveStatus : Swift.Int {
  case NONE
  case LIVENESS_TIMED_OUT
  case SUCCESS
  case BAD_REQUEST
  case UNAUTHORIZED
  case INSUFFICIENT_CREDITS
  case FORBIDDEN
  case NOT_FOUND
  case INTERNAL_SERVER_ERROR
  case ERROR
  case REQUEST_TIMED_OUT
  case INVALID_ERROR
  case RECORDS_ERROR
  case MAX_RETRIES_ERROR
  case CONSENT_ERROR
  case MULTI_RECORDS_ERROR
  case NOT_SUPPORTED_ERROR
  case INTERNAL_RESOURCE_ERROR
  case MANY_RECORDS_ERROR
  public func getMessage() -> Swift.String
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public enum FaceStatus : Swift.Int {
  case VALID_FACE
  case WARNING_FACE
  case INVALID_FACE
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@_hasMissingDesignatedInitializers public class KLiveResult {
  public var livenessScore: Swift.Double
  public var imageB64String: Swift.String
  public var requestId: Swift.String
  @objc deinit
}
extension UIKit.UIImage {
  public func correctlyOrientedImage() -> UIKit.UIImage
}
public enum KEnvironment : Swift.String {
  case DEV
  case ALPHA
  case BETA
  case TEST
  case PROD
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
extension Liveness.FaceTypeStatus : Swift.Equatable {}
extension Liveness.FaceTypeStatus : Swift.Hashable {}
extension Liveness.Brightness : Swift.Equatable {}
extension Liveness.Brightness : Swift.Hashable {}
extension Liveness.Brightness : Swift.RawRepresentable {}
extension Liveness.KLiveStatus : Swift.Equatable {}
extension Liveness.KLiveStatus : Swift.Hashable {}
extension Liveness.KLiveStatus : Swift.RawRepresentable {}
extension Liveness.FaceStatus : Swift.Equatable {}
extension Liveness.FaceStatus : Swift.Hashable {}
extension Liveness.FaceStatus : Swift.RawRepresentable {}
extension Liveness.KEnvironment : Swift.Equatable {}
extension Liveness.KEnvironment : Swift.Hashable {}
extension Liveness.KEnvironment : Swift.RawRepresentable {}
