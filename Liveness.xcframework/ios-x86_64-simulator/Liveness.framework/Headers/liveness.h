//
//  ios_liveness_ml.h
//  ios_liveness_ml
//
//  Created by sanjay.k@karza.in on 16/09/21.
//

#import <Foundation/Foundation.h>

//! Project version number for ios_liveness_ml.
FOUNDATION_EXPORT double ios_liveness_mlVersionNumber;

//! Project version string for ios_liveness_ml.
FOUNDATION_EXPORT const unsigned char ios_liveness_mlVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ios_liveness_ml/PublicHeader.h>


#import "NativeLib.h"
