//
//  NativeLib.h
//  MLTest
//
//  Created by sanjay.k@karza.in on 19/04/21.
//

#import <UIKit/UIKit.h>

@interface NativeLib : NSObject
+(void)initDetector:(NSString*)facePath landmarkPath:(NSString*)landmarkPath;
+(void)initDetector:(NSString*)facePath;
+(int)getBrightness:(float)r g:(float)g b:(float)b;
+(NSArray<NSNumber*>*)getFaces:(unsigned char *)pixels width:(int)width height:(int)height channel:(int)channel;
+(NSArray<NSNumber*>*)getLandmarks:(unsigned char *)pixels width:(int)width height:(int)height channel:(int)channel x:(int)x y:(int)y rect_width:(int)rect_width rect_height:(int)rect_height;
+(float)getQAScore:(unsigned char *)pixels width:(int)width height:(int)height channel:(int)channel x:(int)x y:(int)y rect_width:(int)rect_width rect_height:(int)rect_height landmarks:(NSArray *)landmarks;
+(int)getQACheckValue:(unsigned char *)pixels width:(int)width height:(int)height channel:(int)channel x:(int)x y:(int)y rect_width:(int)rect_width rect_height:(int)rect_height landmarks:(NSArray *)landmarks score:(float)score;
+(float)setQAFaceBrightnessLowMT:(float)face_brightness_low_mt;
+(float)setQAFaceBrightnessHighMT:(float)face_brightness_high_mt;
+(int)setQAFaceSizeMT:(int)face_size_mt;
+(float)setQAFacePoseRMT:(float)face_pose_rmt;
+(float)setQAFacePoseYMT:(float)face_pose_ymt;
+(float)setQAFacePosePMT:(float)face_pose_pmt;
+(float)setQAFaceBlurMT:(float)face_blur_mt;
+(void)destroyDetector;
@end
